'use strict';

var gulp = require('gulp');
// var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var clean = require('gulp-clean');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');

// css compiler
var compass = require('compass-importer');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
// js compiler 
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
// html compiler
var jade = require('gulp-jade');

// Variables de chemins
var source = './src'; // dossier de travail
var destination = './dist'; // dossier à livrer
var vendor = './bower_components'; // vendor packages
var docs = './docs'; // docs folder

// Tâche "css"
gulp.task('css', function () {
    return gulp.src(source + '/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            importer: compass,
            outputStyle: 'nested',
            includePaths: './bower_components/'
        }).on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(destination + '/css/'));
});
gulp.task('cssMin', function () {
    return gulp.src(destination + '/css/*.css')
        .pipe(cleanCSS({keepSpecialComments : 0, processImport : false}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(destination + '/css/'));
});

// Tâche "js"
gulp.task('js', function() {
    return gulp.src([
            vendor + '/bootstrap-sass/assets/javascripts/bootstrap.js',
            vendor + '/moment/min/moment-with-locales.js',
            vendor + '/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js',
            vendor + '/seiyria-bootstrap-slider/src/js/bootstrap-slider.js',
            vendor + '/DataTables/media/js/jquery.dataTables.js',
            vendor + '/DataTables/media/js/dataTables.bootstrap.js',
            vendor + '/jQuery-Mask-Plugin/dist/jquery.mask.js',
            vendor + '/jquery-validation/dist/jquery.validate.js',
            vendor + '/jquery-validation/src/localization/messages_fr.js',
            source + '/js/*.js'
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('stylebox.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(destination + '/js/'));
});
gulp.task('jsMin', function () {
    return gulp.src(destination + '/js/*.js')
        .pipe(uglify())
        .pipe(rename({
            suffix: ".min",
          }))
        .pipe(gulp.dest(destination + '/js/'));
});

// Tâche "fonts"
gulp.task('fonts', function() {
    return gulp.src(vendor + '/fontawesome/fonts/*')
        .pipe(gulp.dest(destination + '/fonts/'));
});

// Tâche "clean"
gulp.task('clean', function () {
    return gulp.src(destination,{ read: false })
    .pipe(clean());
});

// Tâche "build"
gulp.task('build', ['css', 'js', 'fonts']);

// Tâche "minify"
gulp.task('minify', ['cssMin', 'jsMin']);


// Tâche "watch"
gulp.task('watch', function() {
    gulp.watch(source + '/scss/**/*.scss', ['css']);
    gulp.watch(source + '/js/**/*.js', ['js']);
});



// DOCS
// ===============================================================================

// Tâche "htmlDocs"
gulp.task('htmlDocs', function() {
  var YOUR_LOCALS = {};
 
  gulp.src(docs + '/jade/*.jade')
    .pipe(jade({
      locals: YOUR_LOCALS,
      pretty: true
    }))
    .pipe(gulp.dest(docs + '/dist/'));
});

// Tâche "cssDocs"
gulp.task('cssDocs', function () {
    return gulp.src(docs + '/scss/*.scss')
        .pipe(sass(
            {
                importer: compass,
                outputStyle: 'compressed',
                includePaths: './bower_components/',
            }).on('error', sass.logError))
        .pipe(cleanCSS({keepSpecialComments : 0, processImport : false}))
        .pipe(gulp.dest(docs + '/dist/css/'));
});

// Tâche "jsDocs"
gulp.task('jsDocs', function() {
    return gulp.src([
            destination + '/js/stylebox.js',
            docs + '/js/*.js'
        ])
        .pipe(concat('docs.js'))
        .pipe(uglify())
        .pipe(gulp.dest(docs + '/dist/js/'));
});

// Tâche "fontsDocs"
gulp.task('fontsDocs', function() {
    return gulp.src(vendor + '/fontawesome/fonts/*')
        .pipe(gulp.dest(docs + '/dist/fonts/'));
});

// Tâche "cleanDocs"
gulp.task('cleanDocs', function () {
    return gulp.src(docs + '/dist/', { read: false })
    .pipe(clean());
});

// Tâche "buildDocs"
gulp.task('buildDocs', ['cssDocs', 'jsDocs', 'fontsDocs', 'htmlDocs']);

// Tâche "watch"
gulp.task('watchDocs', function() {
    gulp.watch(docs + '/scss/**/*.scss', ['cssDocs']);
    gulp.watch(docs + '/js/**/*.js', ['jsDocs']);
    gulp.watch(docs + '/jade/**/*.jade', ['htmlDocs']);
});


// Global tasks
// ===============================================================================

// Tâche "compileAll"
gulp.task('cleanAll', ['clean', 'cleanDocs']);

// Tâche "buildAll"
gulp.task('buildAll', function(cb) {
  runSequence('build', 'minify', 'buildDocs', cb);
});

// Tâche "watchAll"
gulp.task('watchAll', ['watch', 'watchDocs']);

// Tâche par défaut
gulp.task('default', function(cb) {
  runSequence('cleanAll', 'buildAll', cb);
});

