$(function(){

  $('.splitview').each(function(){
    $(this).append('<div class="splitview-overlay" />');
  });

  $('.splitview-close, .splitview-overlay').click(function(e){
    e.preventDefault();
    $(this).closest('.splitview').removeClass('is-splitview-nav-visible');
  });

  $('.splitview-open').click(function(e){
    e.preventDefault();
    $(this).closest('.splitview').addClass('is-splitview-nav-visible');
  });

});