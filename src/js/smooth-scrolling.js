
// SMOOTH SCROLLING
// ================

$.fn.hasAttr = function(name) {  
   return this.attr(name) !== undefined;
};

$(document).on('click', '[data-scrollto]', function(e) {
	var link = $(this),
		offset = link.hasAttr('data-scrollto-offset') ? link.attr('data-scrollto-offset') : 0,
		speed = link.hasAttr('data-scrollto-speed') ? parseInt(link.attr('data-scrollto-speed')) : 400,
		target = $(link.attr('data-scrollto')),
		parent = (link.hasAttr('data-scrollto-container') && $(link.attr('data-scrollto-container')).length) ? $(link.attr('data-scrollto-container')).first() : $('html,body');
	if (target.length) {
		e.preventDefault();
		parent.animate({
			scrollTop: parent.scrollTop() + (target.offset().top - parent.offset().top) - offset
		}, speed, 'linear');
		return false;
	}
});
