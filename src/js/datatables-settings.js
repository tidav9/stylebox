$.extend( true, $.fn.dataTable.defaults, {
    language: {
        processing:     "Traitement en cours...",
        search:         "Rechercher : ",
        lengthMenu:     "Afficher _MENU_ éléments",
        info:           "_START_ à _END_ sur _TOTAL_ éléments",
        infoEmpty:      "0 élément",
        infoFiltered:   "<small>(filtré sur _MAX_ éléments au total)</small>",
        infoPostFix:    "",
        loadingRecords: "Chargement en cours...",
        zeroRecords:    "Aucun élément trouvé",
        emptyTable:     "Aucune donnée disponible",
        paginate: {
            first:      '<i class="icon icon-angle-double-left"></i>',
            previous:   '<i class="icon icon-angle-left"></i>',
            next:       '<i class="icon icon-angle-right"></i>',
            last:       '<i class="icon icon-angle-double-right"></i>',
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
    }
} );

$(function(){
	$('[data-datatable]').each(function(){
    	var inlineSettingsStr = $(this).attr('data-datatable')
    	var inlineSettings = inlineSettingsStr ? eval("(" + inlineSettingsStr + ")") : {};
    	$(this).DataTable( inlineSettings );
	});
});