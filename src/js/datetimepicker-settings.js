
// DATETIMEPICKER
// ==============

$.extend( true, $.fn.datetimepicker.defaults, {
	locale: 'fr',
	showTodayButton: true,
	showClear: true,
	showClose: true,
	toolbarPlacement: 'top',
	icons: {
		time: "icon icon-clock-o",
		date: "icon icon-calendar",
		up: "icon icon-arrow-up",
		down: "icon icon-arrow-down",
		previous: "icon icon-arrow-left",
		next: "icon icon-arrow-right",
		today: 'icon icon-crosshairs',
		clear: 'icon icon-trash',
		close: 'icon icon-times'
	},
	tooltips: {
		today: 'Aller à aujourd\'hui',
		clear: 'Vider',
		close: 'Fermer',
		selectTime: 'Sélectionner l\'heure',
		selectMonth: 'Sélectionner le mois',
		prevMonth: 'Mois précédent',
		nextMonth: 'Mois suivant',
		selectYear: 'Sélectionner l\'année',
		prevYear: 'Année précédente',
		nextYear: 'Année suivante',
		selectDecade: 'Sélectionner la décade',
		prevDecade: 'Décade précédente',
		nextDecade: 'Décade suivante',
		prevCentury: 'Siècle précédent',
		nextCentury: 'Siècle suivant'
	},
	format: 'DD/MM/YYYY'
});


$(function () {

	$('[data-datetimepicker]').each(function(){
		var inlineSettingsStr = $(this).attr('data-datetimepicker')
		var inlineSettings = inlineSettingsStr ? eval("(" + inlineSettingsStr + ")") : {};

		$(this).datetimepicker(
			$.extend({}, {sideBySide: true, format: 'DD/MM/YYYY HH:mm'}, inlineSettings)
		);
	});

	$('[data-datepicker]').each(function(){
		var inlineSettingsStr = $(this).attr('data-datepicker')
		var inlineSettings = inlineSettingsStr ? eval("(" + inlineSettingsStr + ")") : {};

		$(this).datetimepicker(inlineSettings);
	});

	$('[data-timepicker]').each(function(){
		var inlineSettingsStr = $(this).attr('data-timepicker')
		var inlineSettings = inlineSettingsStr ? eval("(" + inlineSettingsStr + ")") : {};

		$(this).datetimepicker(
			$.extend({}, {format: 'HH:mm'}, inlineSettings)
		);
	});

});