## Documentation

[https://github.alptis.local/pages/davidp/stylebox/](https://github.alptis.local/pages/davidp/stylebox/)

## Installation

Installer avec bower :
```
bower install git://github.alptis.local/davidp/stylebox.git
```

Compiler les sources :
```
cd bower_components/stylebox
npm install
```

Exemple de nouveau projet :
[https://github.alptis.local/davidp/stylebox-start/](https://github.alptis.local/davidp/stylebox-start/)
